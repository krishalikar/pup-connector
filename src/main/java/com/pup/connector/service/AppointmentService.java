package com.pup.connector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pup.connector.dto.AppointmentDTO;
import com.pup.connector.model.Appointment;
import com.pup.connector.repository.AppointmentRepository;

@Service
public class AppointmentService {

	@Autowired
	AppointmentRepository repo;

	public AppointmentDTO getAppointment(String appointmentID) {
		Appointment appointment = repo.findById(appointmentID).get();
		AppointmentDTO appointmentDTO = new AppointmentDTO();

		appointmentDTO.setAppointmentID(appointment.getAppointmentID());
		appointmentDTO.setName(appointment.getName());

		return appointmentDTO;
	}

}