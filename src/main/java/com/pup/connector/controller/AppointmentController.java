package com.pup.connector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pup.connector.dto.AppointmentDTO;
import com.pup.connector.service.AppointmentService;

@RestController
public class AppointmentController {

	@Autowired
	AppointmentService appointmentService;

	@GetMapping("/appointment")
	public AppointmentDTO getAppointment(@RequestParam("appointmentID") String appointmentID) {
		return appointmentService.getAppointment(appointmentID);
	}
}
