package com.pup.connector.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="appointments")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Appointment {


	@Id
	@Column(name="appointmentID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String appointmentID;
	
	@Column(name="name")
	private String name;

}
