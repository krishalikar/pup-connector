package com.pup.connector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pup.connector.model.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, String> {

}
